---
title: "Instalând h-encore"
---

{% include toc title="Cuprins" %}

### Lectură obligatorie

Lanțul de exploits de h-encore pentru PS Vita (TV) permite instalarea de aplicații homebrew pentru ecranul de pornire. It is compatible with the firmware versions 3.65 and 3.68.

Țineți cont că lanțul de exploit-uri h-encore nu este "persistent" (ceea ce înseamnă că nu rămâne instalat după o repornire). Pentru dispo în versiunea de firmware 3.65, aceasta este, din fericire, doar o restricție temporară până când Ensō este instalat într-o pagină ulterioară. For devices on 3.68, you will, unfortunately, need to perform a small portion of these instructions (which will be indicated) after every reboot until a new persistent exploit is developed.

Înainte de a instala exploit-ul h-encore, vom configura setările DNS pe reţea pentru a utiliza un server de actualizare personalizat care blochează noi versiuni de firmware din a se instala atunci când dispozitivul încearcă să efectueze o actualizare.

În plus, faţă de instalarea exploit-ului h-encore, vom activa accesul la homebrew "nesigur" care dă permisiuni extinse la aplicaţiile homebrew. Această idee ar putea fi considerată similară cu modul de "administrator" de pe un calculator.

Dacă aveți un PS Vita 1000, trebuie de asemenea să aveți un card de memorie Sony (de orice mărime) pentru a urma acest ghid. Această restricţie nu se aplică la PS Vita 2000 sau PS TV de vreme ce aceste dispozitive au un card de memorie incorporat.

Unealta "finalhe" este folosită pentru a automatiza instalarea h-encore.

### Ce aveți nevoie

* Ultima versiune de [finalhe](https://github.com/soarqin/finalhe/releases/latest)

### Instrucțiuni

#### Section I - finalhe

1. Copiați conținutul arhivei `.zip` către un folder de pe calculator
1. Porniți finalhe pe calculator
  + Dacă folosiți un calculator Windows și firewall-ul vă cere să permiteți accesul la rețea pentru finalhe, faceți astfel
1. Lansați aplicația Content Manager pe dispozitiv
1. Selectați "Copy Content"
1. Selectați "PC"
1. Selectați metoda cu care doriți să vă conectați la finalhe
  + Dacă vi se cere să vă logați la un cont de PlayStation Network, faceți astfel
  + Dacă nu aveți un cont PlayStation Network, creați unul
1. Selectați / înregistrați calculatorul când vi se cere
  + Dacă vedeți un mesaj care vă cere să actualizați, reporniţi dispozitivul și încercați din nou
  + Dacă dispozitivul nu este detectat prin USB pe Windows, instalați [QcmaDriver_winusb](https://github.com/soarqin/finalhe/releases/download/v1.3/QcmaDriver_winusb.exe) și încercați din nou

#### Section II - h-encore Transfer

1. Selectați "Trim h-encore to ~13MB"
1. Selectați "Let's GO!"
  + Fișierele de exploit vor fi descărcate și pregătite automatic
  + Acest proces va lua ceva timp
1. Selectați "PC -> PS Vita System" pe dispozitiv
1. Selectați "Applications"
1. Selectaţi "PS Vita"
1. Selectaţi "h-encore"
1. Selectați "Copy"
1. Selectați "OK"
  + Exploit-ul h-encore va fi copiat pe dispozitiv
  + Acest proces va lua ceva timp
1. Închideți aplicația Content Manager pe dispozitiv
1. Close finalhe on your computer

___

### Continuați la [Configurând h-encore](configuring-h-encore)
{: .notice--primary}