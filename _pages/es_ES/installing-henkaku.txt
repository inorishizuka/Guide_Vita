---
title: "Installing HENkaku"
---

{% include toc title="Tabla de contenidos" %}

### Lectura requerida

HENkaku is an exploit chain for the PS Vita (TV) which will allow for the installation of homebrew applications to the LiveArea Screen. It is only compatible with the firmware version 3.60.

Note that the HENkaku exploit chain is not "persistent" (meaning it does not remain installed after a reboot). Fortunately, this is only a temporary restriction until HENkaku Ensō is installed on the next page.

In addition to running the HENkaku exploit, we will also enable access to "unsafe" homebrew which gives extended permissions to homebrew applications. This idea could be considered analagous to the "administrator" mode on a computer.

Si tienes una PS Vita 1000, también deberás poseer una tarjeta de memoria oficial de Sony (de cualquier tamaño) para seguir esta guía. Esta restricción no aplica a los modelos PS Vita 2000 o PS TV debido a que poseen una tarjeta de memoria interna.
{: .notice--info}

### Qué necesitas

* Una conexión a internet en tu PS Vita (TV)

### Instrucciones

#### Section I - Launching HENkaku

1. Launch the browser and go to the following URL on your device:
  + `http://henkaku.xyz`
1. Tap "Install"
1. Read the disclaimer, then tap "Install"
  + If you get an error and cannot proceed, [follow this troubleshooting guide](troubleshooting#a-browser-based-exploit-is-not-working)
1. If the exploit was successful, you will now have a bubble on your LiveArea screen named "molecularShell".

#### Sección II - Configurar HENkaku

1. Inicia la aplicación Ajustes
1. Ve a `HENkaku Settings`
1. Activa "Enable Unsafe Homebrew"
1. Vuelve al menú de ajustes de HENkaku
1. Cierra la aplicación Ajustes

___

### Continue to [Installing Ensō (3.60)](installing-enso-(3.60))
{: .notice--primary}
