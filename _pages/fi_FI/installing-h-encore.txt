---
title: "Installing h-encore"
---

{% include toc title="Table of Contents" %}

### Required Reading

The h-encore exploit chain for the PS Vita (TV) allows for the installation of homebrew applications to your home screen. It is compatible with the firmware versions 3.65 and 3.68.

Note that the h-encore exploit chain is not "persistent" (meaning it does not remain installed after a reboot). For devices on firmware version 3.65, this is fortunately only a temporary restriction until Ensō is installed on a later page. For devices on 3.68, you will, unfortunately, need to perform a small portion of these instructions (which will be indicated) after every reboot until a new persistent exploit is developed.

Prior to installing the h-encore exploit, we configure your network's DNS settings to use a custom update server which blocks new firmware versions from being installed when the device attempts to perform an update.

In addition to installing the h-encore exploit, we enable access to "unsafe" homebrew which gives extended permissions to homebrew applications. This idea could be considered analogous to the "administrator" mode on a computer.

If you have a PS Vita 1000, you must also have an official Sony memory card (of any size) to follow this guide. This restriction does not apply to the PS Vita 2000 or PS TV as those devices have a built-in memory card.

The "finalhe" tool is used to automate the h-encore installation.

### What You Need

* The latest release of [finalhe](https://github.com/soarqin/finalhe/releases/latest)

### Instructions

#### Section I - finalhe

1. Copy the contents of the finalhe `.zip` to a folder on your computer
1. Launch finalhe on your computer
  + If you are using a Windows computer and are prompted to allow finalhe network access through the firewall, do so
1. Launch the Content Manager application on your device
1. Select "Copy Content"
1. Select "PC"
1. Select the method you wish to use to connect to finalhe
  + If you are prompted to log-in to a PlayStation Network Account, do so
  + If you do not have a PlayStation Network Account, create one
1. Select / register your computer if prompted
  + If you encounter a message instructing you to update, reboot your device and try again
  + If your device is not detected over USB on Windows, install [QcmaDriver_winusb](https://github.com/soarqin/finalhe/releases/download/v1.3/QcmaDriver_winusb.exe) and try again

#### Section II - h-encore Transfer

1. Select "Trim h-encore to ~13MB"
1. Select "Let's GO!"
  + The exploit files will be automatically downloaded and prepared
  + This process will take some time
1. Select "PC -> PS Vita System" on your device
1. Select "Applications"
1. Select "PS Vita"
1. Select "h-encore"
1. Select "Copy"
1. Select "OK"
  + The h-encore exploit will be copied to your device
  + This process will take some time
1. Close the Content Manager application on your device
1. Close finalhe on your computer

___

### Continue to [Configuring h-encore](configuring-h-encore)
{: .notice--primary}